//
//  AppDelegate.h
//  Single View
//
//  Created by Sandra Koning on 27/06/2014.
//  Copyright (c) 2014 Sandra Koning. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
