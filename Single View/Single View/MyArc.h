//
//  MyArc.h
//  Single View
//
//  Created by Sandra Koning on 27/06/2014.
//  Copyright (c) 2014 Sandra Koning. All rights reserved.
//
// header file
//  interface - internal variable names
//  declaration of functions and variables

#import <Foundation/Foundation.h>

@class ViewController;


@interface MyArc : NSObject


//@property (nonatomic, assign) ViewController *vc;

@property(nonatomic)   double centreX; //_centreX
@property(nonatomic)   double centreY; //_centreY

@property(nonatomic)   double radius; //_radius

@property(nonatomic)   double arcHeight; //_arcHeight

@property(nonatomic)  CGPoint centrePoint; //_centrePoint

-(CGPoint)makeCentrePoint:(double)x y:(double)y;
-(void)setCentrePoint:(CGPoint)point;
+(void)updateDisplay:(ViewController *)vc;

@end
