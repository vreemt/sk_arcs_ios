//
//  MyArc.m
//  Single View
//
//  Created by Sandra Koning on 27/06/2014.
//  Copyright (c) 2014 Sandra Koning. All rights reserved.
//
// main file
//  implementation - internal variable names
//  methods and getters/setters for variables

#import "ViewController.h"
#import "MyArc.h"

@implementation MyArc
+(void)updateDisplay:(ViewController *)vc {
    NSLog(@"MyArc updateDisplay");
    vc.label02.text = @"Update from MyArc";
    //[vc updateLabel01:_centreX]; //no instances available for class methods
    double newX = 3.4;
    [vc updateLabel01:newX];
    NSLog(@"Update from MyArc - x %f",newX);

}


-(CGPoint) _centrePoint {
    NSLog(@"MyArc return centrePoint %f, %f ",_centrePoint.x, _centrePoint.y);
    return _centrePoint;
}

-(void) setCentrePoint:(CGPoint)point {
    NSLog(@"MyArc setCentrePoint was %f,%f - new  %f, %f ",_centrePoint.x, _centrePoint.y, point.x, point.y);
    _centrePoint = point;
}
-(CGPoint) makeCentrePoint:(double)x y:(double)y {
    
    NSLog(@"MyArc makeCentrePoint x %.2f, y %.2f  ",x,y);

    CGPoint centre = CGPointMake(x, y);
    //CGFloat x, GCFloat y
    _centrePoint = centre;
    return centre;
}

/*
 -(double) _centreX {
 NSLog(@"MyArc return centreX %f ",_centreX);
 return _centreX;
 }
-(void)setRadius:(float)r  {
    printf("MyArc setRadius radius was %f will be %f \n",_radius,r);
    _radius = r;
}
-(float)r {
    printf("MyArc r - get radius %f \n",_radius);
    return _radius;
}
-(void)setCentreXYZ:(float)cX y:(float)cY z:(float)cZ {
    printf("MyArc setCentreXYZ was %f %f %f will be %f %f %f   \n",_centreX,_centreY,_centreZ,cX,cY,cZ);
    
    _centreX = cX;
    _centreY = cY;
    _centreX = cZ;
}
-(float)cX {
    printf("MyArc cX - get centreX %f \n",_centreX);
    return _centreX;
}
-(float)cY {
    printf("MyArc cY - get centreY %f \n",_centreY);
    return _centreY;
}
-(float)cZ {
    printf("MyArc cZ - get centreZ %f \n",_centreZ);
    return _centreZ;
}
 */
@end
