//
//  SKDelegateHandler.h
//  Single View
//
//  Created by Sandra Koning on 11/07/2014.
//  Copyright (c) 2014 Sandra Koning. All rights reserved.
//

#import <Foundation/Foundation.h>

// Protocol definition starts here
@protocol SKDelegateProtocol <NSObject>
@required
- (void) processCompleted;
@end
// Protocol Definition ends here


@interface SKDelegateHandler : NSObject
{
    // Delegate to respond back
    id <SKDelegateProtocol> _delegate;
    
}
@property (nonatomic,strong) id delegate;
@property (nonatomic) int number;
@property (nonatomic) NSString *flag;


-(void)startNewProcess; // Instance method

@end