//
//  SKDelegateHandler.m
//  Single View
//
//  Created by Sandra Koning on 11/07/2014.
//  Copyright (c) 2014 Sandra Koning. All rights reserved.
//

#import "SKDelegateHandler.h"

@implementation SKDelegateHandler

-(void)startNewProcess{
    NSLog(@"SKDelegateHandler - startNewProcess");
    [NSTimer scheduledTimerWithTimeInterval:3.0 target:self.delegate
                                   selector:@selector(processCompleted) userInfo:nil repeats:NO];
}
-(void)setFlag:flag{
    NSLog(@"SKDelegateHandler - setFlag");
    _flag = flag;
}
-(NSString *)_flag{
    NSLog(@"SKDelegateHandler - getFlag");
    return _flag;
}
@end
