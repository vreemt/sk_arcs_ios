//
//  ViewController.h
//  Single View
//
//  Created by Sandra Koning on 27/06/2014.
//  Copyright (c) 2014 Sandra Koning. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKDelegateHandler.h"


@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *label01;
@property (weak, nonatomic) IBOutlet UILabel *label02;
@property (weak, nonatomic) IBOutlet UIButton *buttonAdd;

-(IBAction)doButtonAdd:(id)sender;
-(void)updateLabel01:(double)centrePoint;
@end
