//
//  ViewController.m
//  Single View
//
//  Created by Sandra Koning on 27/06/2014.
//  Copyright (c) 2014 Sandra Koning. All rights reserved.
//
//  //SK [object methodName: paramName]


#import "ViewController.h"
#import "MyArc.h"

@interface ViewController ()

@end

@implementation ViewController
//@synthesize labelupdate01;

- (IBAction)doButtonAdd:(id)sender {
    NSLog(@"SK doButtonAdd");
   
    [MyArc updateDisplay:self];
    //[@selector(revealToggle:)];
}
- (void)viewDidLoad
{
    NSLog(@"SK viewDidLoad");
    
    //do stuff
    CGFloat x = 50;
    CGFloat y = 50;
    CGPoint centre = CGPointMake(x+10,y+10);
    //create Arc
    MyArc *newArc = [[MyArc alloc] init]; //makeSphere(centrePoint, myRadius);
    CGPoint new = [newArc makeCentrePoint:x y:y];
    [newArc setCentrePoint:new];
    [newArc setCentrePoint:centre];
    //[_newArc _centreX] = 3.4;
    
    SKDelegateHandler *delegateProtocol = [[SKDelegateHandler alloc]init];
    delegateProtocol.delegate = self;
    [_label02 setText:@"Processing..."];
    [delegateProtocol startNewProcess];
	
    // Do any additional setup after loading
    NSLog(@"SK viewDidLoad Label %@ ",_label01.text);
    _label01.text = @"ViewController - viewDidLoad";
    
    [_buttonAdd addTarget:self
                   action:@selector(doButtonAdd:)
       forControlEvents:UIControlEventTouchUpInside];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

}

- (void)didReceiveMemoryWarning
{    
    NSLog(@"SK didReceiveMemoryWarning");

    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
-(void)updateLabel01:(double)centrePoint {
    NSLog(@"updateLabel01 in VC from MyArc - centrePoint %f", centrePoint);
    _label01.text = [NSString stringWithFormat:@"in VC from MyArc - centrePoint %f", centrePoint];
    ;
}
-(void)processCompleted: flag{
    [_label01 setText:[NSString stringWithFormat:@"Process Completed %@ ",flag]];
}

@end
