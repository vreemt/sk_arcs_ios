//
//  main.m
//  Single View
//
//  Created by Sandra Koning on 27/06/2014.
//  Copyright (c) 2014 Sandra Koning. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
//#import "MyArc.h"

int main(int argc, char * argv[])
{
    NSLog(@"SK main - initialised");
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
    //do stuff
    /* NOT HERE but in ViewController
    CGFloat x = 50;
    CGFloat y = 50;
    CGPoint centre = CGPointMake(x,y);
    //create Arc
    MyArc *newArc = [[MyArc alloc] init]; //makeSphere(centrePoint, myRadius);
    //[newArc makeCentrePoint:x y:y];
    //[newArc setCentrePoint:centre];
     */
}
