readme.md

# Treehouse iOS Basics - Project templates

* src: <http://teamtreehouse.com/library/objectivec-basics/fundamentals-of-c/anatomy-of-a-c-program>

_C is a simple, general purpose programming language. Every C program must contain a main function that is the starting point of execution._

## My stuff

* Using a Single View template
* Make sure a valid code signing cert is configured (select Team)
* Added MyCircle creating during Treehouse tutorials
* TODO create properties `@property(nonatomic) ...`

